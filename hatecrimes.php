<?php
/*
Plugin Name: Hate Crime
Description: Custom Post Type Hate Crime for crimenesdelodio.info project
Author: Gerald Kogler
Author URI: http://go.yuri.at
Text Domain: hatecrimes
*/

wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-ui-css', plugins_url('/lib/jquery-ui.min.css', __FILE__ ));

/* load textdomain */
add_action( 'init', 'hatecrime_load_textdomain' );
function hatecrime_load_textdomain() {
	load_plugin_textdomain('hatecrimes', false, dirname(plugin_basename(__FILE__)));
}

/* create custom post type HATE CRIME */
add_action( 'init', 'create_post_type_hatecrime' );
function create_post_type_hatecrime() {

	register_post_type( 'hatecrime',
		array(
			'labels' => array(
				'name' => __( 'Hate Crimes', 'hatecrimes' ),
				'singular_name' => __( 'Hate Crime', 'hatecrimes' )
			),
			'public' => true,
			"show_in_rest" => true,
			'has_archive' => false,
			'taxonomies' => array( 'type'),
			'supports' => array( 'title', 'editor', 'thumbnail' ),
		)
	);
	register_taxonomy(
		'type',
		'hatecrime',
		array(
			'label' => __( 'Type', 'hatecrimes' ),
			'rewrite' => array( 'slug' => 'type' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy(
		'sentence_type',
		'hatecrime',
		array(
			'label' => __( 'Sentence', 'hatecrimes' ),
			'rewrite' => array( 'slug' => 'sentence_type' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy(
		'delict',
		'hatecrime',
		array(
			'label' => __( 'Delict', 'hatecrimes' ),
			'rewrite' => array( 'slug' => 'delict' ),
			'hierarchical' => true,
		)
	);
}

/* multilanguage hatecrime fields */
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5f8b50b97fdc5',
	'title' => 'Detalles del crimen de odio (multiidioma)',
	'fields' => array(
		array(
			'key' => 'field_5f8b5b692c029',
			'label' => __( 'Crime date', 'hatecrimes' ),
			'name' => 'fecha_del_crimen',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'd/m/Y',
			'return_format' => 'd/m/Y',
			'first_day' => 1,
		),
		array(
			'key' => 'field_5f8b50c8a5892',
			'label' => __('Judicial body', 'hatecrimes'),
			'name' => 'organo_judicial',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5f8b50e5a5893',
			'label' => __('Sentence', 'hatecrimes'),
			'name' => 'sentencia',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 4,
			'new_lines' => '',
		),
		/*array(
			'key' => 'field_5f8d748387741',
			'label' => 'Location',
			'name' => 'location',
			'type' => 'map_field',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'center_lat' => '40.463667',
			'center_lng' => '-3.74922',
			'zoom' => 6,
		),*/
		array(
			'key' => 'field_5f8b50e5a58xx',
			'label' => __('Toot', 'hatecrimes'),
			'name' => 'toot',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 4,
			'new_lines' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'hatecrime',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
    'show_in_rest' => 1,
));

endif;

function add_hatecrime_meta_boxes() {
	add_meta_box("hatecrime_meta", __("Hate Crime details", 'hatecrimes'), "add_hatecrime_details_hatecrime_meta_box", "hatecrime", "normal", "low");
}

function add_hatecrime_details_hatecrime_meta_box() {
	wp_nonce_field(basename(__FILE__), "meta-box-nonce");

	global $post;
	$custom = get_post_custom( $post->ID );
 
	?>	<style>.width99 {width:99%;}</style>
	<p>
		<label><?php _e('Date', 'hatecrimes')?>:</label> 
		<input type="text" class="datepicker" name="date" value="<?= @$custom['date'][0] ?>" />
	</p>
	<p>
		<label><strong><?php _e('Adress', 'hatecrimes')?>:</strong> </label><br />

		<label><?php _e('Street', 'hatecrimes')?>:</label>
		<input type="text" name="street" value="<?= @$custom['street'][0] ?>" /><br />

		<label><?php _e('Neighbourhood', 'hatecrimes')?>:</label>
		<input type="text" name="neighbourhood" value="<?= @$custom['neighbourhood'][0] ?>" /><br />

		<label><?php _e('City', 'hatecrimes')?>:</label>
		<input type="text" name="city" value="<?= @$custom['city'][0] ?>" /><br />

		<label><?php _e('Province', 'hatecrimes')?>:</label>
		<select name="province">
			<option value=""> </option> 

			<?php
				$provinces = getProvinces();
				foreach ($provinces as $province) {
					echo '<option value="'.$province.'" ';
					if (@$custom["province"][0] == $province) echo 'selected="selected"';
					echo '>'.$province.'</option>';
				}
			?>

		</select>
	</p>
	<p>
		<label><strong><?php _e('Location', 'hatecrimes')?>:</strong> </label><br />
		<label><?php _e('Latitude', 'hatecrimes')?>:</label> 
		<input type="text" class="map-field-input-lat" name="latitude" value="<?= @$custom['latitude'][0] ?>" /><br />
		<label><?php _e('Longitude', 'hatecrimes')?>:</label> 
		<input type="text" class="map-field-input-lng" name="longitude" value="<?= @$custom['longitude'][0] ?>" />
	</p>

	<p>
		<label><?php _e('Judicial body', 'hatecrimes')?>:</label> 
		<input type="text" name="trial" value="<?= @$custom['trial'][0] ?>" class="width99 translate" />
	</p>
	<p>
		<label><?php _e('Sentence', 'hatecrimes')?>:</label> 
		<input type="text" name="sentence" value="<?= @$custom['sentence'][0] ?>" class="width99 translate" />
	</p>
	<p>
		<label><?php _e('Age of aggressor', 'hatecrimes')?>:</label> 
		<input type="text" name="age" value="<?= @$custom['age'][0] ?>" />
	</p>
	<p>
		<label><?php _e('Videos', 'hatecrimes')?>:</label><br />
		<textarea name="videos" rows="5" cols="80"><?= @$custom['videos'][0] ?></textarea>
	</p>
	<!--<p>
		<label><?php _e('Sources', 'hatecrimes')?>:</label><br />
		<textarea name="sources" rows="10" cols="80"><?= @$custom['sources'][0] ?></textarea>
	</p>-->

	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.datepicker').datepicker({
			defaultDate: "-1y",
			changeMonth: true,
			changeYear: true,
			yearRange: '1990:2024',
			dateFormat : 'mm/dd/yy'
		});
	});
	</script>
	<?php
}
/**
 * Save custom field data when creating/updating posts
 */
function save_hatecrime_custom_fields() {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;
 
    if(!current_user_can("edit_post", $post_id))
        return $post_id;
 
    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

  global $post;
 
  if ( $post ) {
    update_post_meta($post->ID, "date", @$_POST["date"]);
    update_post_meta($post->ID, "street", @$_POST["street"]);
    update_post_meta($post->ID, "neighbourhood", @$_POST["neighbourhood"]);
    update_post_meta($post->ID, "city", @$_POST["city"]);
    update_post_meta($post->ID, "province", @$_POST["province"]);
    update_post_meta($post->ID, "latitude", @$_POST["latitude"]);
    update_post_meta($post->ID, "longitude", @$_POST["longitude"]);
    update_post_meta($post->ID, "trial", @$_POST["trial"]);
    update_post_meta($post->ID, "sentence", @$_POST["sentence"]);
    update_post_meta($post->ID, "age", @$_POST["age"]);
    //update_post_meta($post->ID, "sources", @$_POST["sources"]);
    update_post_meta($post->ID, "videos", @$_POST["videos"]);
  }
}
add_action( 'admin_init', 'add_hatecrime_meta_boxes' );
add_action( 'save_post', 'save_hatecrime_custom_fields' );

//activate jquery validation plugin
function add_jquery_validation_hatecrime() {
    wp_enqueue_script(
		'jquery-validate',
		plugin_dir_url( __FILE__ ) . '/lib/jquery.validate.min.js',
		array('jquery'),
		'1.11.2',
		true
	);
}
add_action( 'wp_enqueue_scripts', 'add_jquery_validation_hatecrime' );

function getProvinces($onlyKeys=true) {

	/* https://www.ine.es/jaxiT3/Tabla.htm?t=2852 */
	/* 2019 */
	$provincias = array(
		'Álava' => 331549, 
		'Albacete' => 388167, 
		'Alicante' => 1858683, 
		'Almería' => 716820, 
		'Asturias' => 1022800, 
		'Ávila' => 157640, 
		'Badajoz' => 673559, 
		'Baleares' => 1149460, 
		'Barcelona' => 5664579, 
		'Bizkaia' => 1152651, 
		'Burgos' => 356958, 
		'Cáceres' => 394151, 
		'Cádiz' => 1240155, 
		'Cantabria' => 581078, 
		'Castellón' => 579962, 
		'Ceuta' => 84777,
		'Ciudad Real' => 495761, 
		'Córdoba' => 782979, 
		'Cuenca' => 196329, 
		'Gerona' => 771044, 
		'Gipuzkoa' => 723576, 
		'Granada' => 914678, 
		'Guadalajara' => 257762, 
		'Huelva' => 521870, 
		'Huesca' => 220461, 
		'Jaén' => 633564, 
		'La Coruña' => 1119596, 
		'La Rioja' => 316798, 
		'Las Palmas' => 1120406, 
		'León' => 460001, 
		'Lérida' => 434930, 
		'Lugo' => 329587, 
		'Madrid' => 6663394, 
		'Málaga' => 1661785, 
		'Melilla' => 86487,
		'Murcia' => 1493898, 
		'Navarra' => 654214, 
		'Orense' => 307651, 
		'Palencia' => 160980, 
		'Pontevedra' => 942665, 
		'Salamanca' => 330119, 
		'Santa Cruz de Tenerife' => 1032983, 
		'Sevilla' => 1942389, 
		'Segovia' => 153129, 
		'Soria' => 88636, 
		'Tarragona' => 804664, 
		'Teruel' => 134137, 
		'Toledo' => 694844, 
		'Valencia' => 2565124, 
		'Valladolid' => 519546, 
		'Zamora' => 172539, 
		'Zaragoza' => 964693
	);

	if (!$onlyKeys)
		return $provincias;

	return array_keys($provincias);
}

/*
 * Add custom column date to hatecrimes backend table and make sortable
 */
function crimenes_hatecrimes_columns($columns) {
    //$columns['crimedate'] = 'Crime date';
    $columns['fechacrimen'] = __( 'Crime date', 'hatecrimes' );
    $columns['fechaaniversario'] = __( 'Anniversary', 'hatecrimes' );
    return $columns;
}
add_filter('manage_hatecrime_posts_columns', 'crimenes_hatecrimes_columns');

function crimenes_show_hatecrimes_columns($name, $post_id) {
    switch ($name) {
        /*case 'crimedate':
           	echo get_post_meta($post_id, "date", true);
            break;*/
        case 'fechacrimen':
           	the_field("fecha_del_crimen", $post_id);
            break;
        case 'fechaaniversario':
           	$date = get_field("fecha_del_crimen", $post_id, false);
           	echo substr($date,6)."/".substr($date,4,2);
            break;
    }
}
add_action('manage_hatecrime_posts_custom_column',  'crimenes_show_hatecrimes_columns', 10, 2);

add_filter( 'manage_edit-hatecrime_sortable_columns', 'crimenes_add_custom_column_make_sortable' );
function crimenes_add_custom_column_make_sortable( $columns ) {
	$columns['fechacrimen'] = 'fechacrimen';
    $columns['fechaaniversario'] = 'fechaaniversario';
	return $columns;
}

add_action( 'pre_get_posts', 'crimenes_orderby_meta' );
function crimenes_orderby_meta( $query ) {
	if(!is_admin())
		return;
 
	$orderby = $query->get( 'orderby');

	//trigger_error(json_encode($query), E_USER_WARNING);
 
	if( 'fechacrimen' == $orderby ) {
		$query->set('meta_key','fecha_del_crimen');
		$query->set('orderby','meta_value');
	}
	else if( 'fechaaniversario' == $orderby ) {
		$query->set('meta_key','fecha_del_crimen');
	}
}

add_filter( 'posts_orderby', 'posts_orderby_fechaaniversario', 10, 2 );
function posts_orderby_fechaaniversario( $orderby, $query ) {
    if ( is_admin() && 'fechaaniversario' === $query->get( 'orderby' ) ) {
        global $wpdb;

        $order = ( 'ASC' === strtoupper( $query->get( 'order' ) ) ) ? 'ASC': 'DESC';

        // This means, we still sort by the date, but we "recreate" the date using
        // the year 2020 (or any leap year), and month and day from the meta value.
        $orderby = "STR_TO_DATE( CONCAT( '2020-',
            MONTH( {$wpdb->postmeta}.meta_value ), '-',
            DAY( {$wpdb->postmeta}.meta_value
        ) ), '%Y-%m-%d' ) $order";
    }

    return $orderby;
}

?>